variables:
  TORGIT: https://git.torproject.org/tor.git
  BUILDUSER: build
  DEBIAN_FRONTEND: "noninteractive"

## caching is disabled as it triggers all sorts of issues according to anarcat (2021-11-13)
#cache:
#  key: apt-cache
#  paths: [ 'apt-cache' ]

.rule_condition_branch_debian: &rule_condition_branch_debian |
  $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH =~ /^debian-(main$|[0-9].*)/ && $CI_COMMIT_BRANCH !~ /^debian-0\.3\.5($|[.-])/

.rule_condition_branch_debian_0_3_5: &rule_condition_branch_debian_0_3_5 |
  $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH =~ /^debian-0\.3\.5($|[.-])/

.rule_condition_tag_debian: &rule_condition_tag_debian |
  $CI_COMMIT_TAG =~ /^debian-tor-/ && $CI_COMMIT_TAG !~ /^debian-tor-0\.3\.5\./

.rule_condition_tag_debian_0_3_5: &rule_condition_tag_debian_0_3_5 |
  $CI_COMMIT_TAG =~ /^debian-tor-0\.3\.5\./

workflow:
  rules:
    - if: *rule_condition_branch_debian
    - if: *rule_condition_branch_debian_0_3_5
    - if: *rule_condition_tag_debian
    - if: *rule_condition_tag_debian_0_3_5

default:
  after_script:
    - rm -rf build-env


.apt-init: &apt-init |
  # APT_CACHE_DIR="${PWD}/apt-cache" &&
  # mkdir -p "${APT_CACHE_DIR}" &&
  cat > /etc/apt/apt.conf.d/90-gitlab << EOF &&
  APT::Install-Recommends "0";
  APT::Acquire::Retries "5";
  APT::Get::Assume-Yes "true";
  // Dir::Cache::Archives "${APT_CACHE_DIR}";
  EOF
  apt-get update &&
  apt-get dist-upgrade


###########################
###########################
## common binary and source build parts

.build_source_common: &build_source_common
  stage: build
  image: debian:stable-slim
  before_script:
    - rm -rf build-env
    - *apt-init
    - apt-get install sudo
    - apt-get install build-essential
    - apt-get install $(cat debian/.debian-ci/"${CI_JOB_NAME}"/build-depends)
    - adduser --system --group --disabled-password --shell /bin/sh --home "$CI_PROJECT_DIR"/build-env "${BUILDUSER}"
  artifacts:
    paths:
      - source-packages
    expire_in: 1 week

.build_binary_common: &build_binary_common
  stage: build
  timeout: 6h
  variables:
    GIT_STRATEGY: none
  tags: [ $RUNNER_TAG ]
  before_script:
    - rm -rf build-env binary-packages
    - *apt-init
    - apt-get install sudo
    - apt-get install build-essential devscripts reprepro fakeroot
  image: ${DOCKER_ARCH}/${OS}:${SUITE}${IMAGE_EXTENSION}

  script:
    - 'echo "SUITE: $SUITE"'
    - 'echo "DOCKER_ARCH: $DOCKER_ARCH"'
    - find source-packages
    - upstream_version="$(head -n1 source-packages/version.txt)"
    - if [ -z "$upstream_version" ]; then echo >&2 "Did not get package version from artifact."; exit 1; fi
    - |
      case "$SUITE" in
        sid) srcchanges="$(ls -1 source-packages/tor_"$upstream_version"*_src.changes | grep -v '~')";;
        *)   srcchanges="$(ls -1 source-packages/tor_"$upstream_version"*"$SUITE"+*_src.changes)";;
      esac
    - 'echo "srcchanges: $srcchanges"'
    - |
      case "$DOCKER_ARCH" in
        amd64) build_selector="-b";;
        *)     build_selector="-B";;
      esac
    - if [ "$(echo "$srcchanges" | wc -l)" != 1 ] || [ -z "$srcchanges" ] ; then echo >&2 "Weird number of changes files found."; exit 1; fi
    - echo "source changes file:"
    - cat "$srcchanges"
    - dsc="$(dcmd --dsc "$srcchanges")"
    - echo "dsc file is ${dsc}"
    - cat "$dsc"

    - adduser --system --group --disabled-password --shell /bin/sh --home "$CI_PROJECT_DIR"/build-env "${BUILDUSER}"

    - sudo -i -u "${BUILDUSER}" mkdir build-tree
    - sudo -i -u "${BUILDUSER}" sh -c "cd build-tree && dpkg-source -x ../../${dsc}"
    - |
      if [ "${SUITE}" = "xenial" ]; then
        (cd "build-env/build-tree/tor-${upstream_version}" &&
         apt-get install \
           debhelper dh-systemd quilt libssl-dev zlib1g-dev libevent-dev asciidoc docbook-xml \
           docbook-xsl xmlto dh-apparmor libsystemd-dev pkg-config libcap-dev dh-autoreconf \
           liblzma-dev libzstd-dev &&
         ( [ "${DOCKER_ARCH}" != 'amd64' ] && [ "${DOCKER_ARCH}" != 'i386' ] || apt install libseccomp-dev )
        ) || exit 1
      else
        (cd "build-env/build-tree/tor-${upstream_version}" && apt-get build-dep . ) || exit 1
      fi
    - sudo -i -u "${BUILDUSER}" sh -c "cd 'build-tree/tor-${upstream_version}' && debuild -rfakeroot -uc -us '$build_selector'"
    - binchanges="$(cd build-env/build-tree && ls -1 *.changes)"
    - if [ "$(echo "$binchanges" | wc -l)" != 1 ] || [ -z "$binchanges" ] ; then echo >&2 "Weird number of changes files produced."; exit 1; fi
    - sudo -i -u "${BUILDUSER}" mkdir RESULT
    - sudo -i -u "${BUILDUSER}" dcmd ln -v "build-tree/${binchanges}" RESULT

    - mkdir binary-packages
    - mv --no-target-directory build-env/RESULT  binary-packages/"${OS}-${DOCKER_ARCH}-${SUITE}"

  artifacts:
    paths:
      - binary-packages/${OS}-${DOCKER_ARCH}-${SUITE}
    expire_in: 1 hour


###########################
###########################
## matrix configuration

.matrix_full: &matrix_full
  - OS: debian
    SUITE: [ 'sid', 'stretch', 'buster', 'bullseye', 'bookworm' ]
    DOCKER_ARCH: ['amd64', 'i386']
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: '-slim'
  - OS: debian
    DOCKER_ARCH: 'arm64v8'
    SUITE: [ 'sid', 'stretch', 'buster', 'bullseye', 'bookworm' ]
    RUNNER_TAG: aarch64
    IMAGE_EXTENSION: '-slim'

  - OS: ubuntu
    SUITE: [ 'xenial', 'bionic' ]
    DOCKER_ARCH: ['amd64', 'i386']
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: [ '' ]
  - OS: ubuntu
    SUITE: [ 'focal', 'hirsute', 'impish', 'jammy' ]
    DOCKER_ARCH: 'amd64'
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: [ '' ]
  - OS: ubuntu
    SUITE: [ 'xenial', 'bionic', 'focal', 'hirsute', 'impish', 'jammy' ]
    DOCKER_ARCH: 'arm64v8'
    RUNNER_TAG: 'aarch64'
    IMAGE_EXTENSION: [ '' ]

.matrix_tor_0_3_5: &matrix_tor_0_3_5
  - OS: debian
    SUITE: [ 'stretch', 'buster' ]
    DOCKER_ARCH: ['amd64', 'i386']
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: '-slim'
  - OS: debian
    DOCKER_ARCH: 'arm64v8'
    SUITE: [ 'stretch', 'buster' ]
    RUNNER_TAG: aarch64
    IMAGE_EXTENSION: '-slim'

  - OS: ubuntu
    SUITE: [ 'xenial', 'bionic' ]
    DOCKER_ARCH: ['amd64', 'i386']
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: [ '' ]
  - OS: ubuntu
    SUITE: 'focal'
    DOCKER_ARCH: 'amd64'
    RUNNER_TAG: amd64
    IMAGE_EXTENSION: [ '' ]
  - OS: ubuntu
    SUITE: [ 'xenial', 'bionic', 'focal' ]
    DOCKER_ARCH: 'arm64v8'
    RUNNER_TAG: 'aarch64'
    IMAGE_EXTENSION: [ '' ]


###########################
###########################
## nightly/per-commit builds

build_source:
  rules:
    - if: *rule_condition_branch_debian
    - if: *rule_condition_branch_debian_0_3_5
  <<: *build_source_common
  variables:
    GIT_STRATEGY: fetch
    GIT_DEPTH: 1
  script:
    - git tag -f this-build
    - sudo -i -u "${BUILDUSER}" mkdir src
    - sudo -i -u "${BUILDUSER}" git clone file://"${CI_PROJECT_DIR}" --branch this-build         --depth 1 src/debian-tor
    - sudo -i -u "${BUILDUSER}" git clone "$TORGIT" src/tor
    # this should not need network now:
    - sudo -i -u "${BUILDUSER}" --preserve-env=CI_COMMIT_BRANCH src/debian-tor/debian/.debian-ci/"${CI_JOB_NAME}"/build-script

    - mv --no-target-directory build-env/RESULT source-packages

build_binary:
  <<: *build_binary_common
  rules:
    - if: *rule_condition_branch_debian
  parallel:
    matrix: *matrix_full
  needs: [ 'build_source' ]

build_binary_0_3_5:
  <<: *build_binary_common
  rules:
    - if: *rule_condition_branch_debian_0_3_5
  parallel:
    matrix: *matrix_tor_0_3_5
  needs: [ 'build_source' ]


###########################
## release builds

build_source-release:
  rules:
    - if: *rule_condition_tag_debian
    - if: *rule_condition_tag_debian_0_3_5
  <<: *build_source_common
  variables:
    # pristine-tar wants the repo
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  script:
    - sudo -i -u "${BUILDUSER}" mkdir src
    - sudo -i -u "${BUILDUSER}" git clone "${CI_PROJECT_DIR}" --branch "${CI_COMMIT_TAG}" src/debian-tor
    - sudo -i -u "${BUILDUSER}" git clone https://git.torproject.org/debian/tor-pristine-upstream.git --branch master                  --depth 1 src/pristine-upstream
    - sudo -i -u "${BUILDUSER}" git clone https://git.torproject.org/debian/tor-pristine-upstream.git --branch pristine-tar-signatures --depth 1 src/pristine-upstream-signatures

    - sudo -i -u "${BUILDUSER}" mkdir keyrings
    - sudo -i -u "${BUILDUSER}" gpg --no-default-keyring --keyring ./keyrings/upstream.gpg --import "${TOR_DEBIAN_RELEASE_KEYRING_UPSTREAM}"
    - sudo -i -u "${BUILDUSER}" gpg --no-default-keyring --keyring ./keyrings/debian.gpg   --import "${TOR_DEBIAN_RELEASE_KEYRING_DEBIAN}"

    # this should not need network now:
    - sudo -i -u "${BUILDUSER}" --preserve-env=CI_COMMIT_TAG src/debian-tor/debian/.debian-ci/"${CI_JOB_NAME}"/build-script

    - mv --no-target-directory build-env/RESULT source-packages

build_binary-release:
  <<: *build_binary_common
  rules:
    - if: *rule_condition_tag_debian
  parallel:
    matrix: *matrix_full
  needs: [ 'build_source-release' ]

build_binary-release_0_3_5:
  <<: *build_binary_common
  rules:
    - if: *rule_condition_tag_debian_0_3_5
  parallel:
    matrix: *matrix_tor_0_3_5
  needs: [ 'build_source-release' ]


###########################
###########################
## install into deb.tpo

deploy:
  stage: deploy
  image: debian:stable-slim
  variables:
    GIT_STRATEGY: none
  cache: []
  before_script:
    - *apt-init
    - apt-get install openssh-client rsync
  script:
    - chmod 0400 "${STATIC_GITLAB_SHIM_SSH_PRIVATE_KEY}"
    - chmod 0444 "${STATIC_GITLAB_SHIM_SSH_HOST_KEYS}"
    - rm -vf source-packages/version.txt
    - >
      rsync
      -e "ssh -i '${STATIC_GITLAB_SHIM_SSH_PRIVATE_KEY}' -o GlobalKnownHostsFile='${STATIC_GITLAB_SHIM_SSH_HOST_KEYS}'"
      -v
      source-packages/* binary-packages/*/*
      tordeb@palmeri.torproject.org:-overridden-server-side-
    - >
      ssh
      -i "${STATIC_GITLAB_SHIM_SSH_PRIVATE_KEY}"
      -o GlobalKnownHostsFile="${STATIC_GITLAB_SHIM_SSH_HOST_KEYS}"
      tordeb@palmeri.torproject.org dinstall

collect_artifacts:
  rules:
    - if: *rule_condition_tag_debian
    - if: *rule_condition_tag_debian_0_3_5
  stage: deploy
  image: debian:stable-slim
  variables:
    GIT_STRATEGY: none
  cache: []
  script:
    - find binary-packages source-packages
    - chmod go-w -R binary-packages source-packages
  artifacts:
    paths:
      - binary-packages
      - source-packages
    expire_in: 1 month
